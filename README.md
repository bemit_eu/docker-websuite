# Build Image

> Inspired by [fauria/docker-lamp](https://github.com/fauria/docker-lamp) 

```bash
docker build -t bemiteu/webbuild .
docker image save -o img.tar bemiteu/webbuild

# Start Container and open bash
docker run --name webbuild --rm -i -t bemiteu/webbuild bash
```

## ENV Vars

This image uses environment variables to allow the configuration of some parameteres at run time:

- `LOG_STDOUT` [Empty string]
    * Accepted values: Any string to enable, empty string or not defined to disable.
    * Description: Output Apache access log through STDOUT, so that it can be accessed through the [container logs](https://docs.docker.com/reference/commandline/logs/).
- `LOG_STDERR` [Empty string]
    * Accepted values: Any string to enable, empty string or not defined to disable.
    * Description: Output Apache error log through STDERR, so that it can be accessed through the [container logs](https://docs.docker.com/reference/commandline/logs/).
- `LOG_LEVEL` [warn] 
    * Accepted values: debug, info, notice, warn, error, crit, alert, emerg
    * Description: Value for Apache's [LogLevel directive](http://httpd.apache.org/docs/2.4/en/mod/core.html#loglevel).
- `ALLOW_OVERRIDE` [All]
    * All, None
    * Accepted values: Value for Apache's [AllowOverride directive](http://httpd.apache.org/docs/2.4/en/mod/core.html#allowoverride).
    * Description: Used to enable (`All`) or disable (`None`) the usage of an `.htaccess` file.
- `DATE_TIMEZONE` [UTC]
    * Accepted values: Any of PHP's [supported timezones](http://php.net/manual/en/timezones.php)
    * Description: Set php.ini default date.timezone directive and sets MariaDB as well.
- `TERM` [dumb]
    * Accepted values: dumb
    * Description: Allow usage of terminal programs inside the container, such as `mysql` or `nano`.

## Port Volumes

The image exposes ports `80`, and exports four volumes:

* `/var/log/httpd`, containing Apache log files.
* `/var/www/html`, used as Apache's [DocumentRoot directory](http://httpd.apache.org/docs/2.4/en/mod/core.html#documentroot).

The user and group owner id for the DocumentRoot directory `/var/www/html` are both 33 (`uid=33(www-data) gid=33(www-data) groups=33(www-data)`).

The user and group owner id for the MariaDB directory `/var/log/mysql` are 105 and 108 repectively (`uid=105(mysql) gid=108(mysql) groups=108(mysql)`).

## Examples

### Create a temporary container for testing purposes:

```
	docker run -i -t --rm bemiteu/websuite bash
```

### Create a temporary container to debug a web app:

```
	docker run --rm -p 8080:80 -e LOG_STDOUT=true -e LOG_STDERR=true -e LOG_LEVEL=debug -v /my/data/directory:/var/www/html bemiteu/websuite
```

### Create a container linking to another [MySQL container](https://registry.hub.docker.com/_/mysql/):

```
	docker run -d --link my-mysql-container:mysql -p 8080:80 -v /my/data/directory:/var/www/html -v /my/logs/directory:/var/log/httpd --name my-lamp-container bemiteu/websuite
```

### Get inside a running container and open a MariaDB console:

```
	docker exec -i -t my-lamp-container bash
	mysql -u root
```