#
# Docker Image for WebBuilds in BitBucket Pipelines

FROM ubuntu:18.04

# Fix debconf warnings upon build
ARG DEBIAN_FRONTEND=noninteractive

RUN apt-get update && apt-get upgrade -y

RUN apt-get install -yq --no-install-recommends \
      gnupg2 \
      curl \
      apt-utils \
      software-properties-common \
      wget \
      git \
      openssl \
      iputils-ping \
      ant \
      ssh-client \
      lftp \
      unzip \
      zip \
      locales \
      ghostscript \
      imagemagick

RUN curl -sL https://deb.nodesource.com/setup_10.x | bash -

RUN add-apt-repository ppa:ondrej/php

RUN apt-get update

RUN apt-get update && apt-get install -yq --no-install-recommends \
    nodejs \
    php7.3 \
    php7.3-bcmath \
    php7.3-bz2 \
    php7.3-cli \
    php7.3-curl \
    php7.3-dba \
    php7.3-fpm \
    php7.3-gd \
    php7.3-gmp \
    php7.3-imap \
    php7.3-intl \
    php7.3-json \
    php7.3-ldap \
    php7.3-mbstring \
    php7.3-mongodb \
    php7.3-mysql \
    php7.3-pgsql \
    php7.3-soap \
    php7.3-sqlite3 \
    php7.3-xml \
    php7.3-xsl \
    php7.3-zip \
    php-redis \
    php-mongodb \
    php-msgpack \
    php-ssh2 \
    php-yaml \
    php-zmq \
    php-imagick \
    mysql-client \
    sqlite3

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

RUN apt-get install apache2 libapache2-mod-php7.3 -y

RUN apt-get install nano tree vim -y

RUN apt-get clean; rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* /usr/share/doc/*

ENV LOG_STDOUT **Boolean** ENV LOG_STDERR **Boolean** ENV LOG_LEVEL warn ENV ALLOW_OVERRIDE All ENV DATE_TIMEZONE UTC ENV TERM dumb COPY index.php /var/www/html/

COPY run-lamp.sh /usr/sbin/

RUN a2enmod rewrite

#RUN ln -s /usr/bin/nodejs /usr/bin/node

RUN chmod +x /usr/sbin/run-lamp.sh
RUN chown -R www-data:www-data /var/www/html

VOLUME /var/www/html
VOLUME /var/log/httpd
VOLUME /etc/apache2

# Set locales
RUN locale-gen en_US.UTF-8 en_GB.UTF-8 de_DE.UTF-8 es_ES.UTF-8 fr_FR.UTF-8 it_IT.UTF-8

# Default to UTF-8 file.encoding
ENV LANG=C.UTF-8 \
    LC_ALL=C.UTF-8 \
    LANGUAGE=C.UTF-8

EXPOSE 80

CMD ["/usr/sbin/run-lamp.sh"]